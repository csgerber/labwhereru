
package edu.uchicago.gerber.labwhereru.jigs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProvidedLocation {

    @SerializedName("latLng")
    @Expose
    private LatLng latLng;

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

}
