package edu.uchicago.gerber.labwhereru;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import edu.uchicago.gerber.labwhereru.jigs.*;

//https://www.viralandroid.com/2015/12/how-to-get-current-gps-location-programmatically-in-android.html
public class MainActivity extends AppCompatActivity {

    private FloatingActionButton fab;

    private LocationManager locationManager;
    private String mprovider;
    private Location location;
    private Criteria criteria;




    private static final String[] LOCATION_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST=1773;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Fragment fragment = MainActivityFragment.getInstance("http://gerber.cs.uchicago.edu/android/");
        swapInFragment(fragment, R.id.container);




        fab =  findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //call to mapquest including key.

                //get the users lat and long.
              //new FetchMapTask().execute(location.getLatitude(), location.getLongitude());
                double[] coords = getCoords();
             //  new FetchMapTask().execute(41.7886, -87.5987);
               new FetchMapTask().execute(coords[0],coords[1]);

            }
        });





    }
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
    }

    private boolean canAccessLocation() {
        return(hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

   private double[] getCoords() {
       double longitude = 41.7886;
       double latitude = -87.5987;

       //this is working.
       if (canAccessLocation()) {
             LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            @SuppressLint("MissingPermission")
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            longitude = location.getLongitude();
             latitude = location.getLatitude();
       } else {
           requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
       }



       return new double[] {latitude, longitude};


   }



    private class  FetchMapTask extends AsyncTask<Double, Void, String>{

        @Override
        protected void onPreExecute() {
            //refreshLocation();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Double... doubles) {



            StringBuilder url = new StringBuilder();
            url.append("http://www.mapquestapi.com/geocoding/v1/reverse?key=3T11xvUyqVnv1mAZc92C8bUfeQuWUozn&location=");
            //pass it in lat and then long
            url.append(doubles[0]);
            url.append(",");
            url.append(doubles[1]);
            url.append("&includeRoadMetadata=true&includeNearestIntersection=true");
            JSONObject jsonObject = null;
            try {
                jsonObject = Util.getJSON(url.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return jsonObject.toString();
        }

        @Override
        protected void onPostExecute(String json) {

            //  http://www.mapquestapi.com/geocoding/v1/reverse?key=3T11xvUyqVnv1mAZc92C8bUfeQuWUozn&location=41.9369880,-87.6688860&includeRoadMetadata=true&includeNearestIntersection=true
            //time to get jiggy with it
            Response response = new Gson().fromJson(json, Response.class);


            StringBuilder snack = new StringBuilder();
            snack.append(response.getResults().get(0).getLocations().get(0).getStreet());
            snack.append(" ");
            snack.append(response.getResults().get(0).getLocations().get(0).getAdminArea5());
            snack.append(", ");
            snack.append(response.getResults().get(0).getLocations().get(0).getAdminArea3());


            Snackbar.make(fab, snack.toString(), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            swapInFragment(MainActivityFragment.getInstance(response.getResults().get(0).getLocations().get(0).getMapUrl()), R.id.container);

            sendDataToFriendlyServer(snack);


            super.onPostExecute(json);
        }
    }

    private void sendDataToFriendlyServer(StringBuilder snack) {
        //let's log out the users contacts
//        for(int nC = 0; nC < contacts.size(); nC ++){
//            Log.d("FRIENDLY", contacts.get(nC));
//        }
        //let's log out the user's location
        Log.d("FRIENDLY", snack.toString());
    }





    private void swapInFragment(Fragment fragment, int containerId){
        FragmentTransaction t = getSupportFragmentManager()
                .beginTransaction();

        t.replace(containerId, fragment);
        t.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
