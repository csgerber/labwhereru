package edu.uchicago.gerber.labwhereru;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public static final String IMAGE = "IMAGE";

    public MainActivityFragment() {
    }

    public static MainActivityFragment getInstance(String urlToImage){
        MainActivityFragment mainActivityFragment= new MainActivityFragment();
        Bundle bundle = new Bundle();
        bundle.putString(IMAGE, urlToImage);
        mainActivityFragment.setArguments(bundle);
        return mainActivityFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);

        Bundle bundle = getArguments();
        String strUrl = bundle.getString(IMAGE, "http://gerber.cs.uchicago.edu/android/");

        WebView webView = view.findViewById(R.id.webView);
        webView.loadUrl(strUrl);
        return view;
    }
}
